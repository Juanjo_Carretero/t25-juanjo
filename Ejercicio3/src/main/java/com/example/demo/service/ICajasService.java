package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.dto.Cajas;


public interface ICajasService {
	
	public List<Cajas> listarCajas(); //Listar All 
	
	public Cajas guardarCaja(Cajas caja);	//Guarda un cliente CREATE
	
	public Cajas cajaXNumReferencia(String numReferencia); //Leer datos de un cliente READ
	
	public Cajas actualizarCaja(Cajas caja); //Actualiza datos del cliente UPDATE
	
	public void eliminarCaja(String numReferencia);// Elimina el cliente DELETE

}
