package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="Cajas")//en caso que la tabla sea diferente
public class Cajas {

	//Atributos de entidad cliente
			@Id
			@GeneratedValue(strategy = GenerationType.IDENTITY)//busca ultimo valor e incrementa desde id final de db
			@Column(name = "numreferencia")
			private String numReferencia;
			@Column(name = "contenido")
			private String contenido;
			@Column(name = "valor")
			private int valor;
			
			
			@ManyToOne
		    @JoinColumn(name="almacen")
		    private Almacenes almacenes;


			public Cajas(String numReferencia, String contenido, int valor, int almacen, Almacenes almacenes) {
				super();
				this.numReferencia = numReferencia;
				this.contenido = contenido;
				this.valor = valor;
				this.almacenes = almacenes;
			}
			
			public Cajas() {

			}

			public String getNumReferencia() {
				return numReferencia;
			}

			public void setNumReferencia(String numReferencia) {
				this.numReferencia = numReferencia;
			}

			public String getContenido() {
				return contenido;
			}

			public void setContenido(String contenido) {
				this.contenido = contenido;
			}

			public int getValor() {
				return valor;
			}

			public void setValor(int valor) {
				this.valor = valor;
			}

			

			public Almacenes getAlmacenes() {
				return almacenes;
			}

			public void setAlmacenes(Almacenes almacenes) {
				this.almacenes = almacenes;
			}

			@Override
			public String toString() {
				return "Cajas [numReferencia=" + numReferencia + ", contenido=" + contenido + ", valor=" + valor
						+ ", almacenes=" + almacenes + "]";
			}

			
			
			
			
			
			
			
			
	
}
