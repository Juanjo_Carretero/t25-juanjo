package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Fabricante;

public interface IFabricanteService {
	
	//Metodos del CRUD
	public List<Fabricante> listarFabricantes();
	
	public Fabricante guardarFabricante(Fabricante fabricante);
	
	public Fabricante fabricanteXID(int id);
	
	public Fabricante actualizarFabricante(Fabricante fabricante_seleccionado);
	
	public void eliminarFabricante(int id);
	
}
