package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IFabricanteDAO;
import com.example.demo.dto.Fabricante;

@Service
public class FabricanteServiceImpl implements IFabricanteService{

	@Autowired
	IFabricanteDAO iFabricanteDao;
	
	@Override
	public List<Fabricante> listarFabricantes() {
		return iFabricanteDao.findAll();
	}

	@Override
	public Fabricante guardarFabricante(Fabricante fabricante) {
		return iFabricanteDao.save(fabricante);
	}

	@Override
	public Fabricante fabricanteXID(int id) {
		return iFabricanteDao.findById(id).get();
	}

	@Override
	public Fabricante actualizarFabricante(Fabricante fabricante_seleccionado) {
		return iFabricanteDao.save(fabricante_seleccionado);
	}

	@Override
	public void eliminarFabricante(int id) {
		iFabricanteDao.deleteById(id);
	}
	
}
