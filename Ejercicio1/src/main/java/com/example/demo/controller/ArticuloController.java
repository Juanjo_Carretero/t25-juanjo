package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.demo.dto.Articulo;
import com.example.demo.service.ArticuloServiceImpl;

@RestController
@RequestMapping("/api")
public class ArticuloController {
	
	@Autowired
	ArticuloServiceImpl artServImpl;
	
	@GetMapping("/articulos")
	public List<Articulo> listarArticulos(){
		return artServImpl.listarArticulos();
	}
	
	@PostMapping("/articulos")
	public Articulo salvarArticulo(@RequestBody Articulo articulo) {
		
		return artServImpl.guardarArticulo(articulo);
	}
	
	@GetMapping("/articulos/{codigo}")
	public Articulo articuloXID(@PathVariable(name="codigo") int codigo) {
		
		Articulo articulo_xid= new Articulo();
		
		articulo_xid=artServImpl.articuloXID(codigo);
		
		System.out.println("Articulo XID: "+articulo_xid);
		
		return articulo_xid;
	}
	
	@PutMapping("/articulos/{codigo}")
	public Articulo actualizarArticulo(@PathVariable(name="codigo")int id,@RequestBody Articulo articulo) {
		
		Articulo articulo_seleccionado= new Articulo();
		Articulo articulo_actualizado= new Articulo();
		
		articulo_seleccionado= artServImpl.articuloXID(id);
		
		articulo_seleccionado.setNombre(articulo.getNombre());
		articulo_seleccionado.setPrecio(articulo.getPrecio());
		articulo_seleccionado.setFabricante(articulo.getFabricante());
		
		
		articulo_actualizado = artServImpl.actualizarArticulo(articulo_seleccionado);
		
		System.out.println("El video actualizado es: "+ articulo_actualizado);
		
		return articulo_actualizado;
	}
	
	@DeleteMapping("/articulos/{codigo}")
	public void eliminarArticulo(@PathVariable(name="codigo")int id) {
		artServImpl.eliminarArticulo(id);
	}
}
