package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.dto.Departamentos;



public interface IDepartamentosService {
	public List<Departamentos> listarDepartamentos(); //Listar All 
	
	public Departamentos guardarDepartamento(Departamentos departamento);	//Guarda un cliente CREATE
	
	public Departamentos departamentoXCodigo(int codigo); //Leer datos de un cliente READ
	
	public Departamentos actualizarDepartamento(Departamentos departamento); //Actualiza datos del cliente UPDATE
	
	public void eliminarDepartamento(int codigo);// Elimina el cliente DELETE
}
