package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Peliculas;
import com.example.demo.service.PeliculasServiceImpl;


@RestController
@RequestMapping("/api")
public class PeliculasController {
	@Autowired
	PeliculasServiceImpl peliculaServideImpl;
	
	@GetMapping("/peliculas")
	public List<Peliculas> listarPeliculas(){
		return peliculaServideImpl.listarPeliculas();
	}
	
	@PostMapping("/peliculas")
	public Peliculas salvarPelicula(@RequestBody Peliculas pelicula) {
		
		return peliculaServideImpl.guardarPeliculas(pelicula);
	}
	
	@GetMapping("/peliculas/{codigo}")
	public Peliculas peliculaXNumReferencia(@PathVariable(name="codigo") int codigo) {
		
		Peliculas pelicula_xcodigo= new Peliculas();
		
		pelicula_xcodigo=peliculaServideImpl.peliculasXCodigo(codigo);
		
		System.out.println("Pelicula X codigo: "+pelicula_xcodigo);
		
		return pelicula_xcodigo;
	}
	
	@PutMapping("/peliculas/{codigo}")
	public Peliculas actualizarPelicula(@PathVariable(name="codigo")int codigo,@RequestBody Peliculas pelicula) {
		
		Peliculas pelicula_seleccionada= new Peliculas();
		Peliculas pelicula_actualizada= new Peliculas();
		
		pelicula_seleccionada= peliculaServideImpl.peliculasXCodigo(codigo);
		
		pelicula_seleccionada.setNombre(pelicula.getNombre());
		pelicula_seleccionada.setCalificacionEdad(pelicula.getCalificacionEdad());


		
		pelicula_actualizada = peliculaServideImpl.actualizarPeliculas(pelicula_seleccionada);
		
		System.out.println("La pelicula actualizada es: "+ pelicula_actualizada);
		
		return pelicula_actualizada;
	}
	
	@DeleteMapping("/peliculas/{codigo}")
	public void eliminarPelicula(@PathVariable(name="codigo")int codigo) {
		peliculaServideImpl.eliminarPeliculas(codigo);
	}
}
