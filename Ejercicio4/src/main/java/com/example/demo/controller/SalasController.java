package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Salas;
import com.example.demo.service.SalasServiceImpl;



@RestController
@RequestMapping("/api")
public class SalasController {
	@Autowired
	SalasServiceImpl salasServideImpl;
	
	@GetMapping("/salas")
	public List<Salas> listarSalas(){
		return salasServideImpl.listarSalas();
	}
	
	@PostMapping("/salas")
	public Salas salvarSala(@RequestBody Salas sala) {
		
		return salasServideImpl.guardarSalas(sala);
	}
	
	@GetMapping("/salas/{codigo}")
	public Salas salaXCodigo(@PathVariable(name="codigo") int codigo) {
		
		Salas sala_xCodigo= new Salas();
		
		sala_xCodigo=salasServideImpl.salasXCodigo(codigo);
		
		System.out.println("Sala XCodigo: "+sala_xCodigo);
		
		return sala_xCodigo;
	}
	
	@PutMapping("/salas/{codigo}")
	public Salas actualizarSala(@PathVariable(name="codigo")int codigo,@RequestBody Salas sala) {
		
		Salas sala_seleccionado= new Salas();
		Salas sala_actualizado= new Salas();
		
		sala_seleccionado= salasServideImpl.salasXCodigo(codigo);
		
		sala_seleccionado.setNombre(sala.getNombre());
		sala_seleccionado.setPeliculas(sala.getPeliculas());

		
		sala_actualizado = salasServideImpl.actualizarSalas(sala_seleccionado);
		
		System.out.println("La sala actualizada es: "+ sala_actualizado);
		
		return sala_actualizado;
	}
	
	@DeleteMapping("/salas/{codigo}")
	public void eliminarSala(@PathVariable(name="codigo")int codigo) {
		salasServideImpl.eliminarSalas(codigo);
	}
	
	
}
